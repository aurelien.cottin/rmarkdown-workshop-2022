#!/bin/sh

pdflatex -shell-escape presentation.tex
pdflatex -shell-escape presentation.tex
rm -f *.aux *.log *.nav *.out *.snm *.toc *.vrb
