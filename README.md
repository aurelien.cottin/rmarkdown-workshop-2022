2h **french** workshop to begin using `Rmarkdown`, to write nice and reproducible reports

Three parts :

1. `Markdown` syntax
2. **`R`**`markdown` syntax and main feature (tables, figures, bibliography)
3. Writing document and make them prettier

---

All code examples and their `HTML` rendering are located in `exemples`.

Rendering was done using `rmarkdown::render` to `HTML` then [`webshot`](https://github.com/wch/webshot/) to `.png`.

`figures` contains external figures used and `exemples_rapportcomplet` whole code used in third part.

---

This work is licensed under a Creative Commons Attribution 4.0 International License.

![https://i.creativecommons.org/l/by/4.0/88x31.png](https://i.creativecommons.org/l/by/4.0/88x31.png)
