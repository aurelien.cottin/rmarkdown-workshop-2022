bloc 1
```{r}
a <- 1 ; a
```
bloc 2
```{r, include=FALSE}
a <- a + 1 ; a
```
bloc 3
```{r, echo=FALSE}
a <- a + 1 ; a
```
bloc 4
```{r, eval=FALSE}
a <- a + 1 ; a
```

