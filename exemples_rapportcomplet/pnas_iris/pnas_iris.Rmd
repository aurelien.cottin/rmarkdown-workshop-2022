---
title: "PNAS sur Iris"

# Use letters for affiliations, numbers to show equal authorship (if applicable) and to indicate the corresponding author
author:
  - name: Alice Anonymous
    affiliation: a,1,2
  - name: Bob Security
    affiliation: a,b
address:
  - code: a
    address: Some Institute of Technology, Department, Street, City, State, Zip
  - code: b
    address: Another University Department, Street, City, State, Zip

date: "04/03/2022"

corresponding_author:
  code: 2
  text: "To whom correspondence should be addressed. E-mail: bob@email.com"

# For footer text
lead_author_surname: Anonymous

## Remove this if not required    
equal_authors:
  code: 1
  text: "A.O.(Author One) and A.T. (Author Two) contributed equally to this work (remove if not applicable)."


author_contributions: |
  Please provide details of author contributions here.

## Remove this if not required
conflict_of_interest: |
  None

abstract: |
  Iris species influence iris sepal length.


significance: |
  Very significant findings on good'ol iris dataset

acknowledgements: |
  Thanks Rmarkdown.

keywords:
  - one
  - two
  - optional
  - optional
  - optional

## must be one of: pnasresearcharticle (usual two-column layout), pnasmathematics (one column layout), or pnasinvited (invited submissions only)
pnas_type: pnasresearcharticle

bibliography: pnas-sample.bib
csl: pnas.csl

## change to true to add optional line numbering
lineno: false

output: rticles::pnas_article
---



```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
library(tidyverse)
```

## Notes

<!-- https://www.zotero.org/styles -->

Introduction au Rmarkdown avec un rapport-jouet sur les données iris [@fisher_use_1936].
Une ACP [@pearson_lines_1901] et une ANOVA seront utilisées pour remplir le document.

## Apercu du jeu de données

```{r head, include=FALSE}
summary(iris)
```

```{r kable}
knitr::kable(head(iris),
             caption = "6 premières valeurs du jeu de données iris",
             format = "latex")
```


## Figure

La classique ACP @pearson_lines_1901 pour présenter un résumé graphique du jeu de données.

```{r fig-ACP, fig.cap= "ACP sur les données iris", fig.width=3.42, fig.height= 3.42}
pca_out <- select(iris, !Species) %>% prcomp()

pca_data <- tibble(PC1=pca_out$x[,1],
                   PC2=pca_out$x[,2],
                   group=iris$Species)

percent_data <- tibble(percent = 100 * pca_out$sdev ** 2 / sum(pca_out$sdev ** 2),
                       PC = seq_along(percent))
pc1_lab <- percent_data$percent[1] %>% round(2) %>% paste("PC1 (",.,"%)")
pc2_lab <- percent_data$percent[2] %>% round(2) %>% paste("PC2 (",.,"%)")

ggplot(pca_data, aes( x = PC1, y = PC2, color = group)) +
  stat_ellipse(alpha = 1/2) +
  geom_point() +
  labs(x = pc1_lab, y = pc2_lab, color = "Espèces") + 
  theme_classic() + theme(text = element_text(size = 6), legend.position = "bottom")
```

Les espèces sont bien séparées (surtout setosa) d'après les mesures sépales/pétales.

## Modèle

Est ce qu'il y a un lien entre la longueur des sépales (`Sepal.Length`) et l'espèce  (`Species`) ?

Avec un petit modèle linéaire :

```{r model, echo = TRUE}
mod  <- lm(formula = Sepal.Length ~ Species,
           data = iris)
```

Et faire une ANOVA (résumé graphique avec `autoplot`).

```{r model-out}
anova_res <- anova(mod)
pval <- anova_res$`Pr(>F)`[1]
knitr::kable(anova_res,
             caption = "Table de l'ANOVA longueur sépale - espèces",
             format = "latex")
```

Et un résumé graphique avec `autoplot` :

```{r fig-model,fig.cap= "Résumé graphique des résidus du modèle linéaire",fig.width=3.42, fig.height= 3.42}
library(ggfortify)
autoplot(mod,size = 0.5,label.size = 2) + theme_classic() + theme(text = element_text(size = 6))
```

## Résultats

La P value est **très** basse ($`r pval`$). Il y a un lien entre la longueur des sépales et l'espèce *(le prix Nobel est proche !)*.

## Bibliographie

<!-- Leave these lines as they are at the end of your .Rmd file to ensure placement of methods & acknowledgements sections before the references-->
\showmatmethods
\showacknow
\pnasbreak
